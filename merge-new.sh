#!/bin/bash -xe

# Helper script to ease corpus merge

# Path to oss-fuzz clone and S2OPC-fuzzing-data clone
oss=oss-fuzz
s2opc=S2OPC-fuzzing-data


function rm_unnamed()
{
    # Remove files in $1 which are sha1 sums
    python3 << EOF
import os
os.chdir('$1')
for x in sorted(filter(lambda x:len(x)==40 and all(c in '0123456789abcdef' for c in x), os.listdir('.'))):
    # print(x)
    os.remove(x)
EOF
}


function build_fuzzers()
{
    # Build fuzzers for merge
    pushd "$oss"
    python3 infra/helper.py build_fuzzers --sanitizer address s2opc
    popd
}


function fetch_corpus()
{
    # Fetch a single corpus for fuzzer $1 and saves it to oss build
    mkdir -p "$oss/build/out/s2opc/old_corpus"
    # If you don't have gsutil, first install it: https://cloud.google.com/storage/docs/gsutil_install
    #  then you may have an access denied here, which is solved by "gcloud auth login"
    gsutil -m cp -r \
           "gs://s2opc-corpus.clusterfuzz-external.appspot.com/libFuzzer/s2opc_$1_fuzzer" \
           "$oss/build/out/s2opc/old_corpus"
}


function extract_bonuses()
{
    # I don't know if these are useful or not
    # You can find on https://oss-fuzz.com/fuzzer-stats,
    #  searching libFuzzer_s2opc_$1_fuzzer, grouping by Day,
    #  more zips of more backups.
    # Not all of test cases are in the main s2opc-corpus bucket as fetched by fetch_corpus...
    #  so it's worth a try :shrug:
    mkdir -p "$oss/build/out/s2opc/old_corpus"
    find . -maxdepth 1 -iname "*$1_fuzzer_*.zip" -exec unzip -n {} -d "$oss/build/out/s2opc/old_corpus/" \;
}


function move_commited_corpus()
{
    # Move known files for fuzzer $1 from commited corpus to being-merged path
    # (copy all then delete unnamed only)
    cp -t "$oss/build/out/s2opc/old_corpus/" "$s2opc/$1/"* && \
    rm_unnamed "$s2opc/$1"
}


function merge()
{
    pushd "$oss"
    mkdir -p "build/out/s2opc/new_corpus"
    # My python seems too old and interpret -merge=1 as an argument to run_fuzzer, instead of considering that it is a fuzzer_arg
    #python3 infra/helper.py run_fuzzer s2opc "$1_fuzzer" /out/new_corpus -merge=1 /out/old_corpus
    # ... so I run it in the docker used to build it...
    docker run --rm -u $UID:$GROUPS -i -e FUZZING_ENGINE=libfuzzer -e SANITIZER=address -e RUN_FUZZER_MODE=interactive -v /users/brameret/work/s2opc/fuzzing/oss-fuzz/build/out/s2opc:/out -t gcr.io/oss-fuzz-base/base-runner run_fuzzer "$1_fuzzer" /out/new_corpus -merge=1 /out/old_corpus
    popd
}


function move_new_corpus()
{
    mv -t "$s2opc/$1/" "$oss/build/out/s2opc/new_corpus/"*
}


function clean()
{
    rm -rf "./$oss/build/out/s2opc/new_corpus"
    rm -rf "./$oss/build/out/s2opc/old_corpus"
}


# Check that paths exist
if [[ ! -d "$oss" ]]; then
    echo "Must run besides $oss and $s2opc"
    echo "Copy me in parent directory of S2OPC-fuzzing-data for instance"
    false  # Don't want to exit if script is sourced
fi


function main()
{
    for fuz in decode parse_tcp_uri server_request sub; do
        fetch_corpus $fuz
        move_commited_corpus $fuz
        extract_bonuses $fuz
        merge $fuz
        move_new_corpus $fuz
        clean
    done
}

main
